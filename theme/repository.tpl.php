<?php
/**
 *
 */

?>

<div class="repository-meta layoutsize-<?php print $layoutsize; ?>">
<div class="infobox">
<ul>
  <li>
    <div class="clone-urls">
      <form class="clone-urls-form">
        <p class="clone-info"><?php print t("Clone & push urls"); ?></p>
        <?php if (!empty($link)) { ?>
          <p class="clone-radio clone-radio-www"><?php print $link ?></p>
        <?php } ?>
        <p class="clone-radio clone-radio-select">
          <label for="http-<?php print $id ?>">
            <select name="url" id="http-<?php print $id ?>" class="http-<?php print $id ?> git-url">
              <?php foreach($repo as $type => $url) { ?>
                <option value="<?php print $url ?>"><?php print $type ?></option>
              <?php } ?>
            </select>
          </label>
        </p>
        <p class="clone-copy">
          <input type="text" value="<?php print $git_repo ?>" id="" readonly="readonly" class="clone-url">
          <a class="clone-help-toggler" id="" href="#">?</a>
        </p>
      </form>
    </div>
    <div style="display: none;" class="clone-help-text" id="">
      <p>Cloning this repository:</p>
      <pre>
git clone <?php print $git_repo ?> <?php print $name ?> 
cd  <?php print $name ?> 
      </pre>
      <p></p>
      <p>Add this repository as a remote to an existing local repository:</p>
      <pre>
git remote add <?php print $name ?> <?php print $git_repo ?> 
git fetch <?php print $name ?> 
git checkout -b my-local-tracking-branch <?php print $name ?>/master_or_other_branch
      </pre>
      <p></p>
    </div>
  </li>
</ul>
</div>
</div>
