<?php

/**
 * Configures gitolite block
 */
function gitolite_block_admin_form() {
  $form = array();

  $form['repo_view'] = array(
    '#type' => 'fieldset',
    '#title' => t('Common settings'),
  );

  $form['repo_view']['gitolite_block_repository_url'] = array(
    '#type' => 'textarea',
    '#title' => t('Domain of repositories using git'),
    '#description' => t("Enter each url path as <em>name</em>|<em>url</em> on a separate line, e.g. GIT|git@git.example.com:"),
    '#default_value' => variable_get('gitolite_block_repository_url', ''),
    '#required' => FALSE,
  );
  
  $form['repo_view']['gitolite_block_viewgit_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL viewgit, repository name is appended'),
    '#default_value' => variable_get('gitolite_block_viewgit_url', ''),
    '#description' => t('Use http://myurl?a=summary&p='),
    '#required' => FALSE,
  );

  $form['repo_view']['gitolite_block_block_size'] = array(
    '#type' => 'select',
    '#title' => t('Small or wide lay-out'),
    '#default_value' => variable_get('gitolite_block_block_size', 'W'),
    '#options' => array(
      'S' => t('Small'),
      'W' => t('Wide'),
    ),
    '#required' => TRUE,
  );
  
  $form['log_parsing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Log parsing'),
  );
  
  $form['log_parsing']['gitolite_block_viewgit_commit_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL viewgit, repository name and hash are appended'),
    '#default_value' => variable_get('gitolite_block_viewgit_commit_url', ''),
    '#description' => t('Use http://myurl?a=commit&p= or http://myurl?a=commitdiff&p='),
    '#required' => FALSE,
  );

  $form['log_parsing']['gitolite_block_use_mirror'] = array(
    '#type' => 'select',
    '#title' => t('Use a mirror location for log parsing'),
    '#default_value' => variable_get('gitolite_block_use_mirror', 0),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#required' => FALSE,
  );

  $form['log_parsing']['gitolite_block_repository_location'] = array(
    '#type' => 'textfield',
    '#title' => t('URL or directory used for log parsing'),
    '#default_value' => variable_get('gitolite_block_repository_location', ''),
    '#description' => t('Use git@git.example.com:'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

