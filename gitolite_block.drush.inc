<?php
/**
 * @file
 * Defines tasks to interact with gitolite administration.
 */

define('GITOLITE_REPOSITORY', sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'gitolite-admin' . DIRECTORY_SEPARATOR);

/**
 * Implementation of hook_drush_command().
 *
 */
function gitolite_block_drush_command() {
  $items = array();

  $items['gitolite-logs'] = array(
    'description' => "Load logs from repos.",
    'arguments' => array(
      'feature' => 'Repo name to parse.',
    ),
    'options' => array(),
    'examples' => array(
      'drush gitolite-logs',
    ),
    'aliases' => array(),
    'callback' => 'gitolite_block_drush_gitolite_logs',
  );

  return $items;
}

/**
 * Check all git logs
 *
 */
function gitolite_block_drush_gitolite_logs() {
  $args = func_get_args();

  $repos = array();
  if (empty($args)) {
    $result = db_query('SELECT field_repository_value AS name
                FROM {content_field_repository}
                WHERE field_repository_value <> \'\'');
    while ($repo = db_fetch_object($result)) {
      $repos[] = $repo->name;
    }
  }
  else {
    $repos = $args;
  }
  foreach ($repos as $repo) {
    gitolite_block_drush_gitolite_logs_repo($repo);
  }
}

/**
 * Check all git logs for a repo
 *
 */
function gitolite_block_drush_gitolite_logs_repo($repo) {
  $repository_location = variable_get('gitolite_block_repository_location', '');
  if (empty($repository_location)) {
    drush_set_error('gitolite_logs', dt('Please check your settings.'));
    return;
  }
  
  drush_print ('Processing ' . $repo);
  $curcwd = getcwd();
  
  // mirror or clone
  $use_mirror = variable_get('gitolite_block_use_mirror', FALSE);
  if (!$use_mirror) {
    if (is_dir(GITOLITE_TMP_DIR)) {
      $result = exec('rm -rf ' . GITOLITE_TMP_DIR);
    }
    drush_print('Cloning ' . $repo . ' to temporary dir ' . GITOLITE_TMP_DIR);
    $command = 'git clone --mirror ' . $repository_location . $repo . ' ' . GITOLITE_TMP_DIR;
    drush_print($command);
    $result = exec($command, $response, $return_code);
    unset($response);
    chdir(GITOLITE_TMP_DIR);
  }
  else {
    if (is_dir($repository_location . $repo . '.git')) {
      chdir($repository_location . $repo . '.git');
    }
    else {
      drush_set_error('gitolite_logs', dt('@repo not found.', array('@repo' => $repo)));
      return;
    }
  }
  
  // get latest know log from database
  $timestamp = db_result(db_query("SELECT MAX(date) FROM {gitolite_log} where repo = '%s'", array($repo)));
  
  if ($timestamp && $timestamp > 0) {
    $timestamp = $timestamp + 1;
    $command = "git rev-list --reverse --pretty=format:'%H%x09%ad%x09%s%x09%ae' --date=iso  --all --after=$timestamp | grep -v '^commit'";
  }
  else {
    $command = "git rev-list --reverse --pretty=format:'%H%x09%ad%x09%s%x09%ae' --date=iso  --all | grep -v '^commit'";
  }
  $result = exec($command, $response, $return_code);
  if (!empty($response)) {
    foreach ($response as $line) {
      $parts = explode("\t", $line);
      $hash = $parts[0];
      $date = $parts[1];
      $message = $parts[2];
      $email = $parts[3];
      
      // Try to lookup uid
      $uid = 0;
      $account = user_load(array('mail' => $email));
      if ($account) {
        $uid = $account->uid;
      }

      // Insert into DB
      $sql = "INSERT IGNORE INTO {gitolite_log} (hash, date, message, email, repo, uid)
        VALUES ('%s', %d, '%s', '%s', '%s', %d);";
      db_query($sql, array(
        $hash,
        strtotime($date),
        $message,
        $email,
        $repo,
        $uid,
      ));
      
      // Parse message for refs
      $pos = strpos($message, 'refs @');
      if ($pos !== FALSE) {
        $text = substr ($message, $pos);
        preg_match_all('/@(\d+)/', $text, $matches);
        if (isset($matches[1])) {
          foreach ($matches[1] as $match) {
            $nid = trim(str_replace(',', '', $match));
            $sql = "INSERT IGNORE INTO {gitolite_log_refs} (gid, nid)
              SELECT gid, %d FROM {gitolite_log} where hash = '%s'"; 
            db_query ($sql, array($nid, $hash));
          }
        }
      }
    }
  }
  chdir($curcwd);
}
