<?php

/**
 * Implementation of hook_menu().
 */
function gitolite_block_menu() {
  $items = array();

  $items['admin/settings/gitolite/block'] = array(
    'title' => 'Gitolite block administration',
    'description' => 'Configure gitolite block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gitolite_block_admin_form'),
    'access arguments' => array('access administration pages'),
    'file' => 'gitolite_block.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function gitolite_block_theme() {
  return array(
    'gitolite_repository' => array(
      'arguments' => array('url' => NULL),
      'template' => 'theme/repository',
    ),
  );
}

/**
 * Implementation of hook_block().
 */
function gitolite_block_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks['gitolite_block']['info'] = t('Gitolite repositories');
    $blocks['gitolite_block']['cache'] = BLOCK_NO_CACHE;
    
    $blocks['gitolite_commits']['info'] = t('Commits');
    $blocks['gitolite_commits']['cache'] = BLOCK_NO_CACHE;
    
    return $blocks;
  }
  elseif ($op == 'view') {
    switch ($delta) {
      case 'gitolite_block':
        return gitolite_block_block_details();
        break;
      case 'gitolite_commits':
        return gitolite_block_commits_details();
        break;
    }
  }
}

function gitolite_block_block_details() {
  if (($node = og_get_group_context()) && node_access('view', $node)) {
    if (isset($node->field_repository[0]['value'])) {
      $repo_methods = variable_get('gitolite_block_repository_methods', '');
      $base_repo = variable_get('gitolite_block_repository_url', '');
      foreach($node->field_repository as $repo) {
        $block['content'] .= theme('gitolite_repository', $repo['value']);
      }
      drupal_add_css(drupal_get_path('module', 'gitolite_block') . '/gitolite_block.css');
      drupal_add_js(drupal_get_path('module', 'gitolite_block') . '/gitolite_block.js');
      $block['subject'] = t('Repositories');
      return $block;
    }
  }
}

function gitolite_block_commits_details() {
  $task = menu_get_object();
  if ($task->type == 'ct_plus_task') {
    if (($group = og_get_group_context()) && node_access('view', $group)) {
      if (isset($group->field_repository[0]['value'])) {
        $base_commit = variable_get('gitolite_block_viewgit_commit_url', '');
        if ($base_commit) {
          $links = array();
          $sql = "SELECT distinct gl.message, gl.hash, gl.uid, gl.repo, u.name FROM {gitolite_log} gl INNER JOIN {gitolite_log_refs} glr on gl.gid = glr.gid LEFT JOIN users u on gl.uid = u.uid WHERE glr.nid = %d ORDER BY gl.date DESC;";
          $commits = db_query($sql, array ($task->nid));
          while ($commit = db_fetch_object($commits)) {
            $url = $base_commit . $commit->repo . '&h=' . $commit->hash;
            $msg = truncate_utf8(check_plain($commit->message), 50, FALSE, TRUE);
            $link = l($msg, $url, array('absolute' => TRUE, 'attributes' => array('title' => check_plain($commit->message))));
            if (isset($commit->uid)) {
              $link .= ' by ' . l($commit->name, 'user/' . $commit->uid);
            }
            $links[] = $link;
          }
          if (!empty($links)) {
            $block['content'] = theme('item_list', $links);
            $block['subject'] = t('Commits');
            return $block;
          }
        }
      }
    }
  }
}

/**
 *
 */
function gitolite_block_preprocess_gitolite_repository(&$variables) {
  $base_repo = variable_get('gitolite_block_repository_url', 'GIT|git@git.example.com:');
  $methods = explode("\n", $base_repo);

  foreach ($methods as $method) {
    list($type, $url) = explode("|", $method);
    if (isset($url)) {
      $variables['repo'][$type] = trim($url) . trim($variables['url']);
    }
    else {
      $variables['repo']['GIT'] = $type . $variables['url'];
    }
  }

  $variables['git_repo'] = current($variables['repo']);

  $base_view = variable_get('gitolite_block_viewgit_url', '');
  if ($base_view) {
    $variables['link'] = l('www', $base_view . $variables['url'], array('absolute' => TRUE));
  }

  $variables['name'] = $variables['url'];
  
  $variables['layoutsize'] = variable_get('gitolite_block_block_size', 'W');
  
}