/**
 * Javascript functions.
 */
(function($) {
  Drupal.behaviors.gitoliteBlock = function() {
    // select url
    $('.clone-url').click(function () {
      this.select();
    });

    //show details
    $('.clone-help-toggler').click(function() {
      $(this).parents('.infobox').find('.clone-help-text').toggle();
      return false;
    });

    // change url
    $(".clone-radio select").each(function() {
      var url = $(this).val();
      $(this).parents('.infobox').find('.clone-copy input').val(url);
    });
    $(".clone-radio select").change(function() {
      var url = $(this).val();
      $(this).parents('.infobox').find('.clone-copy input').val(url);
    });
  }
})(jQuery)